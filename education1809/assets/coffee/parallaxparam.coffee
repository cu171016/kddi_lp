ua = navigator.userAgent
if ua.indexOf('iPhone') > 0 or ua.indexOf('Android') > 0 and ua.indexOf('Mobile') > 0
  # スマートフォン用コード
  deviceType = 'smp'
else if ua.indexOf('iPad') > 0 or ua.indexOf('Android') > 0
  # タブレット用コード
  deviceType = 'tab'
else
  # PC用コード
  deviceType = 'other'
# parallax
$(document).ready ->
  $('#parallax1').parallax '50%', 0.6
  $('#parallax2').parallax '50%', 0.6
  $('#parallax3').parallax '50%', 0.6
  $('#parallax4').parallax '50%', 0.6
  $('#parallax5').parallax '50%', 0.6
  return
# タブレットやスマートフォン時に非対応要素「background-attachment」を「scroll」へ変更
#$ ->
#  if deviceType is 'smp' or deviceType is 'tab'
#    $('#parallax1').css 'background-attachment', 'scroll'
#    $('#parallax2').css 'background-attachment', 'scroll'
#    $('#parallax3').css 'background-attachment', 'scroll'
#    $('#parallax4').css 'background-attachment', 'scroll'
#    $('#parallax5').css 'background-attachment', 'scroll'
#    return