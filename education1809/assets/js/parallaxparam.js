(function() {
  var deviceType, ua;

  ua = navigator.userAgent;

  if (ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
    deviceType = 'smp';
  } else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
    deviceType = 'tab';
  } else {
    deviceType = 'other';
  }

  $(document).ready(function() {
    $('#parallax1').parallax('50%', 0.6);
    $('#parallax2').parallax('50%', 0.6);
    $('#parallax3').parallax('50%', 0.6);
    $('#parallax4').parallax('50%', 0.6);
    $('#parallax5').parallax('50%', 0.6);
  });

}).call(this);
