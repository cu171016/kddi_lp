$(function(){

	// Page top
	var topBtn = $('.pagetop a');    
	topBtn.hide();
	//スクロールが100に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
		//ボタンの表示方法
			topBtn.fadeIn();
		} else {
		//ボタンの非表示方法
			topBtn.fadeOut();
		}
	});
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,  html').animate({
				scrollTop: 0
		},   500);
		return false;
	});
	
	$('.pagetop').click(function () {
		return false;
	});


	// アニメーション
	var wow = new WOW({ callback: ftAnimation });
	wow.init();
	var intervalList = [];
	$('.f_coinAnimTrigger').hover( ftAnimation, function(){});


	$(window).load(function() {
		$('.main_img').addClass('main_animation');
	});

	function ftAnimation () {

		if ( $('.f_coin').hasClass('exBounceInDown')) $('.f_coin').removeClass('animated exBounceInDown');
		if ( $('.f_st').hasClass('exBurst')) $('.f_st').removeClass('animated exBurst');

		$('.f_coin').each(function (idx, el){
			$(el).css({ 'animation-delay': idx * 0.04 + 's' });
		});
		$('.f_st').each(function (idx, el){
			$(el).css({ 'animation-delay': '3s' });
		});

		setTimeout(function (){
			$('.f_coin').addClass('animated exBounceInDown');
			$('.f_st').addClass('animated exBurst');
		}, 200);
	}
});